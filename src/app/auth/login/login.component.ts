import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { SignupComponent } from '../signup/signup.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginFormGroup: FormGroup;

  //form controls
  userName: FormControl;
  password: FormControl;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.registerFormGroup()
  }

  registerFormGroup = () => {
    this.loginFormGroup = new FormGroup({
      userName: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    })
  }

  openSignupPopup = () => {
    let dialogRef = this.dialog.open(SignupComponent, {
      width: '50rem',
      height: '58rem',
      data: ''
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   console.log(result)
    // });
  }
}
