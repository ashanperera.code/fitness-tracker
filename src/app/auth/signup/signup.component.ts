import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  hide: boolean = false;

  constructor(public dialogRef: MatDialogRef<SignupComponent>,@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.dialogRef.disableClose = !this.dialogRef.disableClose;
  }

  closeSignUpModal = () => {
    this.dialogRef.disableClose = !this.dialogRef.disableClose;
    this.dialogRef.close();
  }

}
